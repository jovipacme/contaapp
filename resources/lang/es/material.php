<?php

return [
    'plural'                => 'Articulos',
    'singular'              => 'Articulo',
    'create'                => 'Crear',
    'actions'               => 'Accion',
    'update'                => 'Actualizar',
    'code'                  => 'Codigo',
    'description'           => 'Descripcion',
    'name'                  => 'Nombre',
    'prices'                => 'Precios',
    'measures'              => 'Presentaciones',
    'providers'             => 'Proveedores',        
    'category_material_id'  => 'Categoria del Articulo',
];
