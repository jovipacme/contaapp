<?php

return [
    'plural'                => 'Presentaciones de los Articulos',
    'singular'              => 'Presentacion del Articulo',
    'material'              => 'Material',
    'create'                => 'Crear',
    'actions'               => 'Accion',
    'update'                => 'Actualizar',
    'measure'               => 'Medida',
    'measure_id'            => 'ID Medida',
    'material_id'           => 'ID Material',
];
