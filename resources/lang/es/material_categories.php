<?php

return [
    'singular'          => 'Categoría del Articulo',
    'plural'            => 'Categorias para Articulos',
    'add_new'           => 'Añadir nueva',
    'id'                => 'Codigo',
    'name'               => 'Nombre',
    'created_at'          => 'Creado el',
    'updated_at'          => 'Actualizado el',
    'deleted_at'          => 'Eliminado el',

];
