<?php

return [
    'plural'            => 'Socios de negocios',
    'singular'          => 'Socio de negocio',
    'name'              => 'Nombre',
    'telephone'         => 'Telefono',
    'email'             => 'Correo electronico',
    'category_id'       => 'Categoria socio',
    'nit'               => 'Nit',
    'create'            => 'Crear',
    'actions'           => 'Accion',
    'update'            => 'Actualizar',
];
