<?php

return [
    'singular'          => 'Presentacion del Producto',
    'plural'            => 'Presentacion de los Productos',
    'add_new'           => 'Añadir nueva',
    'id'                => 'Codigo',
    'name'               => 'Nombre',
    'unit'               => 'Unidad',    
    'created_at'          => 'Creado el',
    'updated_at'          => 'Actualizado el',
    'deleted_at'          => 'Eliminado el',

];
