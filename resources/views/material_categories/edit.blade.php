@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.edit') }} {{__('material_categories.plural')}}
@stop

@section('main-content')

    <h1>{{ __('generic.edit') }} {{__('material_categories.plural')}}</h1>
    <hr/>

    {!! Form::model($material_category, [
        'method' => 'PATCH',
        'url' => ['material_categories', $material_category->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text( __('material_categories.name'), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit( __('generic.save') , ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection