@extends('layouts.app')
@section('htmlheader_title')
{{ __('material_categories.plural') }} 
@stop

@section('main-content')

    <h1> {{ __('material_categories.plural') }}  <a href="{{ url('material_categories/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add_new') }} {{ __('material_categories.singular') }}</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbladmin-material_categories">
            <thead>
                <tr>
                    <th>{{ __('material_categories.id') }}</th><th>Name</th><th>{{ __('generic.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($material_categories as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('material_categories', $item->id) }}">{{ $item->name }}</a></td>
                    <td>
                        <a href="{{ url('material_categories/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">{{ __('generic.edit') }}</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['material_categories', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit(__('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbladmin-material_categories').DataTable({
            columnDefs: [{
                targets: [0],
                visible: true,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection