@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.edit') }} {{ __('material.singular') }}
@stop

@section('main-content')

    <h1>{{ __('generic.edit') }} {{ __('material.singular') }}</h1>
    <hr/>

    {!! Form::model($material, [
        'method' => 'PATCH',
        'url' => ['material', $material->id],
        'class' => 'form-horizontal'
    ]) !!}

            <div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
                {!! Form::label('code', __('material.code'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('code', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', __('material.description'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required', 'rows' => '2']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('material_category_id') ? 'has-error' : ''}}">
                {!! Form::label('material_category_id', __('material.category_material_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('material_category_id', $categoriesList, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('material_category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit( __('material.update'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
        <div class="col-sm-3">
            {{ link_to(PreviousRoute::getNamedRoute('material.index.back'), __('generic.cancel'), ['class' => 'btn btn-primary form-control']) }}
        </div>        
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

@endsection