@extends('layouts.app')
@section('htmlheader_title')
Material_measure
@stop

@section('main-content')

    <h1>Material_measure</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Material</th><th>Measure</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{ $material_measure->id }} </td>
                    <td> {{ $material_measure->material->description }} </td>
                    <td> {{ $material_measure->measure->name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection