@extends('layouts.app')

@section('htmlheader_title')
	{{ trans('message.home') }}
@endsection

@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                <div class="jumbotron">
                    <h2>{{ trans('message.logged') }}</h2>
                    <p class="lead">{{ setting('site.title') }}</p>
                </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
