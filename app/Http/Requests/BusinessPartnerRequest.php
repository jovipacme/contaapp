<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusinessPartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $rules = [
            'name' => 'required',
            'telephone' => "nullable|regex:/^([\d-\s,]*)+$/",
            'email' => 'nullable|email',
            'category_id' => 'required',
            'nit'=>'nullable|alpha_dash'
        ];
        return $rules;
    }
}
