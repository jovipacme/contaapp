<?php

namespace App\Http\Controllers;

use Session;
use App\Measure;

use App\Material;
use Carbon\Carbon;
use App\Http\Requests;
use App\Material_Category;
use App\Helpers\PreviousRoute;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MaterialRequest;

class MaterialController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $material = Material::all();

        PreviousRoute::setNamedRoute('material.index.back');
        return view('material.index', compact('material'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categoriesList = Material_Category::pluck('name', 'id');
        
        return view('material.create', compact('categoriesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MaterialRequest $request)
    {
        $this->validate($request, ['description' => 'required', ]);

        Material::create($request->all());

        Session::flash('message', 'Material added!');
        Session::flash('status', 'success');

        return redirect('material');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $material = Material::findOrFail($id);

        return view('material.show', compact('material'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $material = Material::findOrFail($id);

        $categoriesList = Material_Category::pluck('name', 'id');

        return view('material.edit', compact('material','categoriesList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, MaterialRequest $request)
    {
        $this->validate($request, ['description' => 'required', ]);

        $material = Material::findOrFail($id);
        $material->update($request->all());

        Session::flash('message', 'Material updated!');
        Session::flash('status', 'success');

        return redirect('material');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $material = Material::findOrFail($id);

        $material->delete();

        Session::flash('message', 'Material deleted!');
        Session::flash('status', 'success');

        return redirect('material');
    }

}
