<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Material_Measure extends Model
{
    use Traits\HasCompositePrimaryKey;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'material_measures';

    /**
     * The primary key of the table.
     *
     * @var array
     */
    protected $primaryKey = ['material_id', 'measure_id'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['material_id', 'measure_id'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $autofillRelationships = ['material_id', 'measure_id'];

    public function material() {
        return $this->belongsTo('App\Material');
    }

    public function measure() {
        return $this->belongsTo('App\Measure');
    }    
}
