<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;

class PermissionsTable2Seeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {

        Permission::generateFor('material_categories');

        Permission::generateFor('measures');

        Permission::generateFor('materials');

        Permission::generateFor('partner_categories');
    }
}
