<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTable2Seeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'admin')->firstOrFail();

        $materialMenuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => __('seeders.menu_items.commodities'),
            'url'     => '',
        ]);
        if (!$materialMenuItem->exists) {
            $materialMenuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-tree',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 15,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => __('seeders.menu_items.categories'),
            'url'     => '',
            'route'   => 'voyager.material-categories.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-categories',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 16,
            ])->save();
        }   

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => __('seeders.menu_items.measure'),
            'url'     => '',
            'route'   => 'voyager.measures.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-watch',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 17,
            ])->save();
        }

        $materialMenuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => __('seeders.menu_items.commodities'),
            'url'     => '',
            'route'   => 'voyager.material.index',
        ]);
        if (!$materialMenuItem->exists) {
            $materialMenuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-leaf',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 18,
            ])->save();
        }

    }
}
